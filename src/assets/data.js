/*
    Represent the data that will be retrieved from the server.
    This data can be edited to test scenarios.
*/

export default {
    "__type": "HRC.QuestionnaireLoadModel",
        "QuestionnaireID": 9,
        "ProcessID": 745289,
        "ProcessTypeID": 1,
        "CounselID": 0,
        "StageID": 35,
        "ChecklistID": 66,
        "InvestigatorID": 1,
        "CaseMode": "W",
        "TrailID": 912169,
        "SubStageFunctionID": 5105,
        "SubStageNavFunctionID": 0,
        "IsReadOnly": false,
        "CompanyID": 90,
        "BaseURL": null,
        "Questions": [
            {
                "ID": 3511,
                "Text": "Did you describe to the employee what the employee is alleged to have done or omitted to do?",
                "Type": 1,
                "Header": "",
                "Options": null,
                "Order": 1,
                "Required": true,
                "Routes": null,
                "Notes": null,
                "Forms": null,
                "Participants": null,
                "RequireTextBox": {
                    "Yes": false,
                    "No": false,
                    "Prompt": "",
                    "Answer": null
                },
                "RequireAttachment": {
                    "Yes": false,
                    "No": false,
                    "Prompt": "",
                    "DescriptionPrompt": "",
                    "DescriptionAnswer": null,
                    "Filename": null,
                    "Attachments": []
                },
                "Answer": null,
                "PromptAnswer": "",
                "AttachmentID": 0,
                "File": null,
                "NotForTrial": false
            },
            {
                "ID": 3512,
                "Text": "What was the employees response and explanation?",
                "Type": 2,
                "Header": "",
                "Options": null,
                "Order": 2,
                "Required": true,
                "Routes": null,
                "Notes": null,
                "Forms": null,
                "Participants": null,
                "RequireTextBox": null,
                "RequireAttachment": null,
                "Answer": "",
                "PromptAnswer": "",
                "AttachmentID": 0,
                "File": null,
                "NotForTrial": false
            },
            {
                "ID": 3513,
                "Text": "Was the employee aware of the Company\u0027s rule or procedure or should the employee have been expected to know whether their action/s or lack thereof were in contravention of a the company\u0027s rule or procedure? (No will result in a no case) ",
                "Type": 1,
                "Header": "",
                "Options": null,
                "Order": 3,
                "Required": true,
                "Routes": null,
                "Notes": null,
                "Forms": null,
                "Participants": null,
                "RequireTextBox": {
                    "Yes": true,
                    "No": false,
                    "Prompt": "How was the employee made aware?",
                    "Answer": null
                },
                "RequireAttachment": {
                    "Yes": false,
                    "No": false,
                    "Prompt": "",
                    "DescriptionPrompt": "",
                    "DescriptionAnswer": null,
                    "Filename": null,
                    "Attachments": []
                },
                "Answer": null,
                "PromptAnswer": "",
                "AttachmentID": 0,
                "File": null,
                "NotForTrial": false
            },
            {
                "ID": 3514,
                "Text": "Is there sufficient evidence to prove the employee\u0027s actions or lack thereof? (No will result in a no case) ",
                "Type": 1,
                "Header": "",
                "Options": null,
                "Order": 4,
                "Required": true,
                "Routes": null,
                "Notes": null,
                "Forms": null,
                "Participants": null,
                "RequireTextBox": {
                    "Yes": true,
                    "No": false,
                    "Prompt": "Please elaborate",
                    "Answer": null
                },
                "RequireAttachment": {
                    "Yes": true,
                    "No": false,
                    "Prompt": "Attach evidence ",
                    "DescriptionPrompt": "Attach evidence ",
                    "DescriptionAnswer": null,
                    "Filename": null,
                    "Attachments": []
                },
                "Answer": null,
                "PromptAnswer": "",
                "AttachmentID": 0,
                "File": null,
                "NotForTrial": false
            },
            {
                "ID": 3629,
                "Text": "Is the offence minor enough to follow the Disciplinary Discussion process ( No will result in a procedure change which will need to be approved) ",
                "Type": 1,
                "Header": "",
                "Options": null,
                "Order": 5,
                "Required": true,
                "Routes": null,
                "Notes": null,
                "Forms": null,
                "Participants": null,
                "RequireTextBox": {
                    "Yes": true,
                    "No": false,
                    "Prompt": "Please elaborate ",
                    "Answer": null
                },
                "RequireAttachment": {
                    "Yes": false,
                    "No": false,
                    "Prompt": "",
                    "DescriptionPrompt": "",
                    "DescriptionAnswer": null,
                    "Filename": null,
                    "Attachments": []
                },
                "Answer": null,
                "PromptAnswer": "",
                "AttachmentID": 0,
                "File": null,
                "NotForTrial": false
            },
            {
                "ID": 601335,
                "Text": "QUESTION",
                "Type": 7,
                "Header": "HEADER",
                "Options": null,
                "Order": 6,
                "Required": true,
                "Routes": null,
                "Notes": null,
                "Forms": null,
                "Participants": null,
                "RequireTextBox": {
                    "Yes": false,
                    "No": false,
                    "Prompt": "",
                    "Answer": null
                },
                "RequireAttachment": {
                    "Yes": false,
                    "No": false,
                    "Prompt": "",
                    "DescriptionPrompt": "",
                    "DescriptionAnswer": null,
                    "Filename": null,
                    "Attachments": [
                        {
                            "__type": "HRC.QuestionnaireFile",
                            "ProcessID": 745289,
                            "ProcessTypeID": 1,
                            "QuestionnaireID": 9,
                            "QuestionID": 601335,
                            "CounselID": 0,
                            "AttachmentID": 473263,
                            "AttachmentFile": "6bf28763-38c2-4c4b-b0ef-99fca7297d3d.pdf",
                            "Description": "jjj",
                            "URL": "http://localhost:1029/Proc/AttachmentsView.aspx?cxPZbjoayMV6MrOx3kFKEQCVwoTk23hHuDfgP77oWeluS11Ox4J%2bCKg8xvMm5Dq5n0uy5XVL9iSK0pyj0luEWbJj8T2z%2b1NMhehiRtj%2fd1tFaQEPLPPARBgbAHgvOK8npwM9lhw6w9Mkr1LZDl0edk6fYpNm%2fv5l",
                            "IsReadOnly": false
                        }
                    ]
                },
                "Answer": "",
                "PromptAnswer": "",
                "AttachmentID": 0,
                "File": null,
                "NotForTrial": false
            },
            {
                "ID": 601336,
                "Text": "555",
                "Type": 1,
                "Header": "",
                "Options": null,
                "Order": 7,
                "Required": true,
                "Routes": [
                    {
                        "RouteID": 181884,
                        "Condition": [
                            {
                                "QuestionID": 601335,
                                "Answer": "3,"
                            }
                        ]
                    }
                ],
                "Notes": [
                    {
                        "RouteID": 32926,
                        "Condition": [
                            {
                                "QuestionID": 601335,
                                "Answer": "3,"
                            }
                        ],
                        "Text": "tyty",
                        "AllApply": false
                    }
                ],
                "Forms": null,
                "Participants": null,
                "RequireTextBox": {
                    "Yes": false,
                    "No": false,
                    "Prompt": "",
                    "Answer": null
                },
                "RequireAttachment": {
                    "Yes": false,
                    "No": false,
                    "Prompt": "",
                    "DescriptionPrompt": "",
                    "DescriptionAnswer": null,
                    "Filename": null,
                    "Attachments": []
                },
                "Answer": null,
                "PromptAnswer": "",
                "AttachmentID": 0,
                "File": null,
                "NotForTrial": false
            }
        ],
        "TypeID": 1,
        "Nature": 788,
        "SubTypeID": 0,
        "Action": 0,
        "NotForTrialLabel": "Not available during the trial"
    }
